#include <stdio.h>
#include <stdlib.h>
#include "main.h"

int main(){
    char input[9];

    printf("Please enter a decimal number (val 0-255) or binary number> ");
    if( fgets( input, 9, stdin ) == NULL )
        return EXIT_FAILURE;

    int inputInteger;
    switch( isValidDigit( input, &inputInteger ) ) {
        case -1:
            printf("[FATAL] Your input was deemed invalid. Terminating execution");
            return EXIT_FAILURE;
        case 'u':
            printf("Your input has given us a bit of trouble, which type of number is it?\n");
            printf("Is your input (b)inary, or (d)ecimal? ");

            break;
        case 'd':
            printf("Value detected as a decimal number, answer is %d in binary.",
                    convert( inputInteger, 1 ));

            return EXIT_SUCCESS;
        case 'b':
            printf("Value detected as a binary number, answer is %d in decimal.",
                    convert( inputInteger, 0 ));

            return EXIT_SUCCESS;
    }

    return EXIT_FAILURE;
}

