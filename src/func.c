#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include "main.h"

int isValidDigit( char *input, int *integerInput ) {
    int nonBinary = 0;
    for(int i = 0; i < sizeof( input ); ++i){
        if( isalpha( input[i] ) )
            return -1;
        else if( isdigit( input[i] ) && ( input[i] - '0' ) > 1 )
            nonBinary = 1;
        else if( input[i] == '\0' )
            break;
    }

    char *inp;
    *integerInput = strtol(input, &inp, 10);
    int isDecimalOutOfBounds = ( *integerInput < 0 || *integerInput > 255 ) ? 1 : 0;

    if( nonBinary || *integerInput == 0 || floor( log10( abs( *integerInput ) ) ) == 0 ){
        if( isDecimalOutOfBounds ) {
            printf("[FATAL] Decimal number provided is out of valid range (0-255)\n");
            return -1;
        }

        return 'd';
    } else if( input[0] == '0' || isDecimalOutOfBounds ){
        return 'b';
    }

    return 'u';
}

/*
 * Accepts an integer target, and a integer (boolean) toBinary.
 *
 * If toBinary is 0, the input is expected to be a binary input, decimal output will
 * be generated
 *
 * If toBinary is 1, he input is expected to be decimal, binary output will be generated.
 *
 * The provided input is expected to be valid.
 */
int convert(int target, int toBinary){
    int output = 0, base = toBinary ? 2 : 10, remainder;
    int i = toBinary ? 1 : 0;

    while( target != 0 ){
        remainder = target % base;
        target /= base;

        output += (toBinary ? ( remainder * i ) : ( remainder * pow( 2, i ) ));

        i = toBinary ? i * 10 : i + 1;
    }

    return output;
}

