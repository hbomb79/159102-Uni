int isValidDigit(char *input, int *integerInput);
int convertDecimalToBinary(int decimal);
int convertBinaryToDecimal(int binaryInput);
int convert(int target, int base);
